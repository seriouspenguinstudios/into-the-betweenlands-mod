
### Class

```zenscript
import mods.roots.Spells;
```

#### Methods

```zenscript
Spell getSpell(
  string spellName // the name of the spell (spell_ will be prepended if not provided)
);
```


---


