
### Class

```zenscript
import mods.roots.predicates.BlockStateBelow;
```

#### Methods

```zenscript
BlockStateBelow create(
  IPredicate predicate // a defined predicate that describes (potentially) multiple blockstates
);
```

Creates an IWorldCondition which tests the state below a block using the provided IPredicate.

---


