
### Class

```zenscript
import mods.roots.predicates.BlockStateAbove;
```

#### Methods

```zenscript
BlockStateAbove create(
  IPredicate predicate // a defined predicate that describes (potentially) multiple blockstates
);
```

Creates an IWorldCondition which tests the state above a block using the provided IPredicate.

---


