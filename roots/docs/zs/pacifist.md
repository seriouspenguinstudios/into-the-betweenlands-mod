
### Class

```zenscript
import mods.roots.Pacifist;
```

#### Methods

```zenscript
void addEntity(
  IEntityDefinition entity // the entity to add to the pacifist list
);
```

Allows the addition of new entities to the list of 'Pacifist' creatures, i.e., those that, when killed, will grant the Untrue Pacifist advancement.

---


```zenscript
void removeEntity(
  IEntityDefinition entity // the entity to remove from the pacifist list
);
```

Removes an entity from the list of 'Pacifist Creatures', i.e., those that, when killed, will grant the Untrue Pacfist advancement.

---


