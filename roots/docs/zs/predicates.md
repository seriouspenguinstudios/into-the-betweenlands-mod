
### Class

```zenscript
import mods.roots.predicates.Predicates;
```

### Static Properties

```zenscript
Predicates.Lava // IPredicate which matches both lava and flowing lava

Predicates.Water // IPredicate which matches both water and flowing water

Predicates.Leaves // IPredicate which matches all types of leaves

```
