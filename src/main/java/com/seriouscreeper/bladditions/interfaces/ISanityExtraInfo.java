package com.seriouscreeper.bladditions.interfaces;

public interface ISanityExtraInfo {
    boolean getCanIncrease();
    boolean getCanDecrease();
    void setCanIncrease(boolean canIncrease);
    void setCanDecrease(boolean canDecrease);
}
