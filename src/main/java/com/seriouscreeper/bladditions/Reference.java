package com.seriouscreeper.bladditions;

public class Reference {
    public static final String CLIENT_PROXY_CLASS = "com.seriouscreeper.bladditions.proxy.ClientProxy";
    public static final String SERVER_PROXY_CLASS = "com.seriouscreeper.bladditions.proxy.ServerProxy";
}
