package com.seriouscreeper.bladditions.blocks;

import com.seriouscreeper.bladditions.BLAdditions;
import com.seriouscreeper.bladditions.tiles.PatchedTileSpa;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.SoundEvents;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fluids.FluidUtil;
import thaumcraft.Thaumcraft;
import thaumcraft.common.blocks.BlockTCDevice;

public class PatchedBlockSpa extends BlockTCDevice {
    public PatchedBlockSpa() {
        super(Material.ROCK, PatchedTileSpa.class, "spa");
        this.setSoundType(SoundType.STONE);
    }

    public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumHand hand, EnumFacing side, float hitX, float hitY, float hitZ) {
        if (world.isRemote) {
            return true;
        } else {
            TileEntity tileEntity = world.getTileEntity(pos);
            if (tileEntity instanceof PatchedTileSpa && !player.isSneaking()) {
                FluidStack fs = FluidUtil.getFluidContained(player.getHeldItem(hand));
                if (fs != null) {
                    PatchedTileSpa tile = (PatchedTileSpa)tileEntity;
                    if (tile.tank.getFluidAmount() < tile.tank.getCapacity() && (tile.tank.getFluid() == null || tile.tank.getFluid().isFluidEqual(fs)) && FluidUtil.interactWithFluidHandler(player, hand, tile.tank)) {
                        player.inventoryContainer.detectAndSendChanges();
                        tile.markDirty();
                        world.markAndNotifyBlock(pos, world.getChunk(pos), state, state, 3);
                        world.playSound((EntityPlayer)null, pos, SoundEvents.ITEM_BOTTLE_FILL, SoundCategory.BLOCKS, 0.33F, 1.0F + (world.rand.nextFloat() - world.rand.nextFloat()) * 0.3F);
                    }
                } else {
                    player.openGui(BLAdditions.instance, 3, world, pos.getX(), pos.getY(), pos.getZ());
                }
            }

            return true;
        }
    }
}
