package com.seriouscreeper.bladditions.rituals;

import epicsquid.mysticallib.network.PacketHandler;
import epicsquid.mysticallib.util.Util;
import epicsquid.roots.entity.ritual.EntityRitualBase;
import epicsquid.roots.network.fx.MessageFrostLandsProgressFX;
import epicsquid.roots.ritual.IColdRitual;
import epicsquid.roots.ritual.RitualRegistry;
import net.minecraft.block.BlockFarmland;
import net.minecraft.block.BlockLiquid;
import net.minecraft.block.BlockSnow;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.monster.EntitySnowman;
import net.minecraft.init.Blocks;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import thebetweenlands.common.registries.BlockRegistry;

import java.util.ArrayList;
import java.util.List;

public class PatchedEntityRitualFrostLands extends EntityRitualBase implements IColdRitual {
    private PatchedRitualFrostLands ritual;


    public PatchedEntityRitualFrostLands(World worldIn) {
        super(worldIn);
        this.getDataManager().register(lifetime, RitualRegistry.ritual_frost_lands.getDuration() + 20);
        this.ritual = (PatchedRitualFrostLands) RitualRegistry.ritual_frost_lands;
    }

    @Override
    protected void entityInit() {
        this.posY = y;
        this.posX = x;
        this.posZ = z;
    }

    @Override
    public void onUpdate() {
        super.onUpdate();

        if (world.isRemote) return;
        List<BlockPos> affectedPositions = new ArrayList<>();

        if (this.ticksExisted % ritual.interval_heal == 0) {
            List<BlockPos> positions = Util.getBlocksWithinRadius(world, getPosition(), ritual.radius_x, ritual.radius_y, ritual.radius_z, (BlockPos pos) -> world.isAirBlock(pos.up()) && !world.isAirBlock(pos) && Blocks.SNOW_LAYER.canPlaceBlockAt(world, pos));
            int breakout = 0;
            while (!positions.isEmpty() && breakout < 50) {
                BlockPos choice = positions.get(rand.nextInt(positions.size()));
                IBlockState choiceState = world.getBlockState(choice);
                if (choiceState.getBlock() != Blocks.SNOW_LAYER) {
                    if (Blocks.SNOW_LAYER.canPlaceBlockAt(world, choice.up())) {
                        world.setBlockState(choice.up(), Blocks.SNOW_LAYER.getDefaultState());
                        affectedPositions.add(choice.up());
                        break;
                    }
                } else if (choiceState.getValue(BlockSnow.LAYERS) < 8 && Util.rand.nextInt(5) == 0) {
                    world.setBlockState(choice, choiceState.withProperty(BlockSnow.LAYERS, Math.min(choiceState.getValue(BlockSnow.LAYERS) + 1, 3)));
                    affectedPositions.add(choice);
                    break;
                }

                breakout++;
            }

            positions = Util.getBlocksWithinRadius(world, getPosition(), ritual.radius_x, ritual.radius_y, ritual.radius_z, (BlockPos pos) -> (world.getBlockState(pos).getBlock() == BlockRegistry.SWAMP_WATER || world.getBlockState(pos).getBlock() == Blocks.LAVA) && world.isAirBlock(pos.up()));
            if (!positions.isEmpty()) {
                BlockPos choice = positions.get(rand.nextInt(positions.size()));
                IBlockState state = world.getBlockState(choice);

                if (state.getBlock() == BlockRegistry.SWAMP_WATER) {
                    if (state.getValue(BlockLiquid.LEVEL) == 0) {
                        world.setBlockState(choice, BlockRegistry.BLACK_ICE.getDefaultState());
                        affectedPositions.add(choice);
                    }
                }
            }

            positions = Util.getBlocksWithinRadius(world, getPosition(), ritual.radius_x, ritual.radius_y, ritual.radius_z, Blocks.FIRE);
            for (BlockPos pos : positions) {
                world.setBlockToAir(pos);
                affectedPositions.add(pos);
            }

            positions = Util.getBlocksWithinRadius(world, getPosition(), ritual.radius_x, ritual.radius_y, ritual.radius_z, Blocks.FARMLAND);
            for (BlockPos pos : positions) {
                IBlockState state = world.getBlockState(pos);
                if (state.getValue(BlockFarmland.MOISTURE) != 7) {
                    world.setBlockState(pos, state.withProperty(BlockFarmland.MOISTURE, state.getValue(BlockFarmland.MOISTURE) + 1));
                    affectedPositions.add(pos);
                }
            }
        }

        if (!affectedPositions.isEmpty()) {
            MessageFrostLandsProgressFX progress = new MessageFrostLandsProgressFX(affectedPositions);
            PacketHandler.sendToAllTracking(progress, this);
        }
    }

    @Override
    protected void readEntityFromNBT(NBTTagCompound compound) {
        this.x = compound.getDouble("x");
        this.y = compound.getDouble("y");
        this.z = compound.getDouble("z");
        if (compound.hasUniqueId("player")) {
            this.setPlayer(compound.getUniqueId("player"));
        }
        this.setEntityId(compound.getInteger("entity_id"));
        this.setPosition(x, y, z);
        getDataManager().set(lifetime, compound.getInteger("lifetime"));
        getDataManager().setDirty(lifetime);
    }

    @Override
    protected void writeEntityToNBT(NBTTagCompound compound) {
        compound.setDouble("x", x);
        compound.setDouble("y", y);
        compound.setDouble("z", z);
        compound.setInteger("entity_id", getEntityId());
        compound.setInteger("lifetime", getDataManager().get(lifetime));
        if (player != null) {
            compound.setUniqueId("player", player);
        }
    }
}
