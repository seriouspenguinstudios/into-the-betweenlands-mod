package com.seriouscreeper.bladditions.items;

import baubles.api.BaubleType;
import baubles.api.IBauble;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.MobEffects;
import net.minecraft.item.EnumRarity;
import net.minecraft.item.IItemPropertyGetter;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagByte;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.translation.I18n;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import thaumcraft.api.items.IRechargable;
import thaumcraft.api.items.RechargeHelper;
import thaumcraft.api.potions.PotionFluxTaint;
import thaumcraft.common.config.ConfigItems;
import thaumcraft.common.items.ItemTCBase;
import thaumcraft.common.items.baubles.ItemVerdantCharm;
import thebetweenlands.api.capability.IDecayCapability;
import thebetweenlands.common.registries.CapabilityRegistry;

import javax.annotation.Nullable;
import java.util.List;

public class PatchedVerdantCharm extends ItemTCBase implements IBauble, IRechargable {
    public PatchedVerdantCharm() {
        super("thaumcraft:verdant_charm", new String[0]);
        this.setTranslationKey("verdant_charm");
        this.maxStackSize = 1;
        this.canRepair = false;
        this.setMaxDamage(0);
        this.addPropertyOverride(new ResourceLocation("type"), new IItemPropertyGetter() {
            @SideOnly(Side.CLIENT)
            public float apply(ItemStack stack, @Nullable World worldIn, @Nullable EntityLivingBase entityIn) {
                return stack.getItem() instanceof ItemVerdantCharm && stack.hasTagCompound() ? (float)stack.getTagCompound().getByte("type") : 0.0F;
            }
        });
    }

    public EnumRarity getRarity(ItemStack itemstack) {
        return EnumRarity.RARE;
    }

    public BaubleType getBaubleType(ItemStack itemstack) {
        return BaubleType.CHARM;
    }

    public void getSubItems(CreativeTabs tab, NonNullList<ItemStack> items) {
        if (tab == ConfigItems.TABTC || tab == CreativeTabs.SEARCH) {
            items.add(new ItemStack(this));
            ItemStack vhbl = new ItemStack(this);
            vhbl.setTagInfo("type", new NBTTagByte((byte)1));
            items.add(vhbl);
            ItemStack vhbl2 = new ItemStack(this);
            vhbl2.setTagInfo("type", new NBTTagByte((byte)2));
            items.add(vhbl2);
        }

    }

    @SideOnly(Side.CLIENT)
    public void addInformation(ItemStack stack, World worldIn, List<String> tooltip, ITooltipFlag flagIn) {
        if (stack.hasTagCompound() && stack.getTagCompound().getByte("type") == 1) {
            tooltip.add(TextFormatting.GOLD + I18n.translateToLocal("item.verdant_charm.life.text"));
        }

        if (stack.hasTagCompound() && stack.getTagCompound().getByte("type") == 2) {
            tooltip.add(TextFormatting.GOLD + I18n.translateToLocal("item.verdant_charm.sustain.text"));
        }

    }

    public void onWornTick(ItemStack itemstack, EntityLivingBase player) {
        if (!player.world.isRemote && player.ticksExisted % 20 == 0 && player instanceof EntityPlayer) {
            if (player.getActivePotionEffect(MobEffects.WITHER) != null && RechargeHelper.consumeCharge(itemstack, player, 20)) {
                player.removePotionEffect(MobEffects.WITHER);
                return;
            }

            if (player.getActivePotionEffect(MobEffects.POISON) != null && RechargeHelper.consumeCharge(itemstack, player, 10)) {
                player.removePotionEffect(MobEffects.POISON);
                return;
            }

            if (player.getActivePotionEffect(PotionFluxTaint.instance) != null && RechargeHelper.consumeCharge(itemstack, player, 5)) {
                player.removePotionEffect(PotionFluxTaint.instance);
                return;
            }

            if (itemstack.hasTagCompound() && itemstack.getTagCompound().getByte("type") == 1 && player.getHealth() < player.getMaxHealth() && RechargeHelper.consumeCharge(itemstack, player, 5)) {
                player.heal(1.0F);
                return;
            }

            if (itemstack.hasTagCompound() && itemstack.getTagCompound().getByte("type") == 2) {
                if (player.getAir() < 100 && RechargeHelper.consumeCharge(itemstack, player, 1)) {
                    player.setAir(300);
                    return;
                }

                if(player instanceof EntityPlayer) {
                    EntityPlayer entityPlayer = (EntityPlayer) player;
                    IDecayCapability cap = player.getCapability(CapabilityRegistry.CAPABILITY_DECAY, null);

                    if (cap != null && cap.getDecayStats().getDecayLevel() > 0 && RechargeHelper.consumeCharge(itemstack, player, 2)) {
                        cap.getDecayStats().addStats(-1, 0);
                    }

                    if (entityPlayer.canEat(false) && RechargeHelper.consumeCharge(itemstack, player, 1)) {
                        entityPlayer.getFoodStats().addStats(1, 0.3F);
                        return;
                    }
                }
            }
        }

    }

    public int getMaxCharge(ItemStack stack, EntityLivingBase player) {
        return 200;
    }

    public EnumChargeDisplay showInHud(ItemStack stack, EntityLivingBase player) {
        return EnumChargeDisplay.NORMAL;
    }

    public boolean willAutoSync(ItemStack itemstack, EntityLivingBase player) {
        return true;
    }
}
